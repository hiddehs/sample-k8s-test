# Digital Ocean / onprem autodevops cluster install


```bash
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
kubectl get secrets
kubectl get secret default-token-hmv8t -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

kubectl apply -f gitlab-admin-service-account.yaml
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
kubectl get pods --all-namespaces
watch kubectl get pods --all-namespaces
```
